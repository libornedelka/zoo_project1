#include <iostream>
#include "Creature.h"
#include "AttackItem.h"
#include "DeffendItem.h"
#include "Hero.h"

int main() {
    std::cout << "Hello, World!" << std::endl;

    Hero* h1 = new Hero("Libor");
    Creature* c1 = new Creature("Marta",150,30);
    AttackItem* i1 = new AttackItem(5,"Dyka","Weapon");
    DeffendItem* i2 = new DeffendItem(3,"Krouzkove brnko","Armor");


    return 0;
}
