//
// Created by Olsal on 16.12.2020.
//

#include "Creature.h"


Creature::Creature(std::string name, int health, int attack) {
    m_name = name;
    m_attack = attack;
    m_health = health;
}

int Creature::getAttack() {
    return m_attack;
}

int Creature::getHealth() {
    return m_health;
}

void Creature::printInfo() {
    std::cout << "Creature name: " << m_name << std::endl;
    std::cout << "Creature attack: " << m_attack << std::endl;
    std::cout << "Creature health: " << m_health << std::endl;
}