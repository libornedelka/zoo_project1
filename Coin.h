//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_COIN_H
#define ZOOPROJECT_COIN_H

#include "Item.h"

class Coin: public Item {
public:
    Coin(int bonus, std::string itemName, std::string typeOfItem);
    int getBonus();
    void printInfo();
};


#endif //ZOOPROJECT_COIN_H
