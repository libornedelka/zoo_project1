//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_DEFFENDITEM_H
#define ZOOPROJECT_DEFFENDITEM_H

#include "item.h"

class DeffendItem : public Item {
public:
    DeffendItem(int bonus, std::string itemName, std::string typeOfItem);
    int getBonus();
    void printInfo();
};


#endif //ZOOPROJECT_DEFFENDITEM_H
