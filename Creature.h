//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_CREATURE_H
#define ZOOPROJECT_CREATURE_H

#include <iostream>
class Creature {

    std::string m_name;
    int m_health;
    int m_attack;
public:
    Creature(std::string name, int health, int attack);
    int getHealth();
    int getAttack();
    void printInfo();


};


#endif //ZOOPROJECT_CREATURE_H
