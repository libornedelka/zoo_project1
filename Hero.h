//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_HERO_H
#define ZOOPROJECT_HERO_H

#include <iostream>
#include "Inventory.h"

class Hero {
    int m_health;
    int m_attack;
    int m_armor;
    int m_numberOfCoins;
    std::string m_name;
public:

    Hero(std::string name);
    int getAttack();
    int getHealth();
    int getArmor();
    void printInfo();

    void setArmor(Inventory* defenseBonus);
    void setAttack(Inventory* attackBonus);

};


#endif //ZOOPROJECT_HERO_H
