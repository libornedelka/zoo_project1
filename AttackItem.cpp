//
// Created by Olsal on 16.12.2020.
//

#include "AttackItem.h"

AttackItem::AttackItem(int bonus, std::string itemName, std::string typeOfItem):Item(bonus,itemName,typeOfItem) {

}

int AttackItem::getBonus() {
    return m_bonus;
}

void AttackItem::printInfo() {
    std::cout << "Item name: " << m_itemName << std::endl;
    std::cout << "Type of item: " << m_typeOfItem << std::endl;
    std::cout << "Bonus: " << m_bonus << std::endl;
}