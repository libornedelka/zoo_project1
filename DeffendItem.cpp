//
// Created by Olsal on 16.12.2020.
//

#include "DeffendItem.h"

DeffendItem::DeffendItem(int bonus, std::string itemName, std::string typeOfItem):Item(bonus, itemName, typeOfItem) {

}

int DeffendItem::getBonus() {
    return m_bonus;
}

void DeffendItem::printInfo() {
    std::cout << "Item name: " << m_itemName << std::endl;
    std::cout << "Type of item: " << m_typeOfItem << std::endl;
    std::cout << "Bonus: " << m_bonus << std::endl;



}