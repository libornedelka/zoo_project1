//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_ITEM_H
#define ZOOPROJECT_ITEM_H

#include <iostream>

class Item {
protected:
    int m_bonus;
    std::string m_itemName;
    std::string m_typeOfItem;

public:
    Item(int bonus, std::string itemName, std::string typeOfItem);
    virtual int getBonus() = 0;
    virtual void printInfo() = 0;
};


#endif //ZOOPROJECT_ITEM_H
