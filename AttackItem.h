//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_ATTACKITEM_H
#define ZOOPROJECT_ATTACKITEM_H

#include "item.h"

class AttackItem: public Item {

public:
    AttackItem(int bonus, std::string itemName, std::string typeOfItem);
    int getBonus();
    void printInfo();

};


#endif //ZOOPROJECT_ATTACKITEM_H
