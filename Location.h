//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_LOCATION_H
#define ZOOPROJECT_LOCATION_H

#include <vector>
#include "Game.h"

class Location {
    std::vector<Item*> m_itemsAtLocation;
    std::vector<Creature*> m_creatureAtLocation;
public:
    Location(/*std::vector itemsAtLocation, std::vector creatureAtLocation*/);

};


#endif //ZOOPROJECT_LOCATION_H
