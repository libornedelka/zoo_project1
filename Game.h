//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_GAME_H
#define ZOOPROJECT_GAME_H

#include <array>
#include "Item.h"
#include "Creature.h"
#include "Location.h"



class Game {

    //std::array<Location*,4> m_listOfLocations;
    std::array<Item*,10> m_listOfItems;
    std::array<Creature*,5> m_listOfCreatures;
public:
    Game();


};


#endif //ZOOPROJECT_GAME_H
